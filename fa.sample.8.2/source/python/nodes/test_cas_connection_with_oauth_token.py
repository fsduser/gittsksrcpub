import os
import logging
import swat

# input_output_df_dict {'IN_INST_DISCOUNT': df_in, 'OUT_INST_DISCOUNT': df_out }
def irm_run(input_output_df_dict):
    logger = logging.getLogger(__name__)
    logger.info('Entering execution of task test_cas_connection_with_oauth_token.py')
    
    # define CAT_CAS_TOKEN
    os.environ['CAS_TOKEN'] = os.environ['MTC_OAUTH_TOKEN']
    conn = swat.CAS(hostname='sas-cas-server-default-client', port=5570)
    logger.info('We connected to CAS successfully using CAS_TOKEN.\nconn: {}'.format(conn))
    
    # or pass the token string  in the call as password
    del os.environ['CAS_TOKEN']
    conn2 = swat.CAS(hostname='sas-cas-server-default-client', port=5570, password=os.environ['MTC_OAUTH_TOKEN'])
    logger.info('We connected to CAS successfully with passing the token as pw.\nconn2: {}'.format(conn2))
    
    logger.info('Exiting execution of task test_cas_connection_with_oauth_token.py.')
