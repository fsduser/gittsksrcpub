import logging
import sys
import os


logger = logging.getLogger(__name__)
TASK_OPRPERTY_DF_NAME = '_irm_task_property_dict_'

# irm_split_filepath('/local/fa.sample.3.6/landing_area/03312019/cashflows.sas7bdat') => ('/local/fa.sample.3.6/landing_area/03312019', 'cashflows', '.sas7bdat')
def irm_split_filepath(path):
    "Output: dir, name, ext"
    dir, filename = os.path.split(path)
    name, ext = os.path.splitext(filename)
    return dir.lower(), name.lower(), ext.lower()


def irm_load_pytask_module(task_code_path):
    "Output: module"
    mod_dir, mod_name, mod_ext = irm_split_filepath(task_code_path)
    if sys.version_info[0] == 3 and sys.version_info[1] >= 5:
        import importlib.util
        spec = importlib.util.spec_from_file_location(mod_name, task_code_path)
        task_mod = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(task_mod)
    elif sys.version_info[0] == 3 and sys.version_info[1] <= 4:
        from importlib.machinery import SourceFileLoader
        task_mod = SourceFileLoader(mod_name, task_code_path).load_module()
    elif sys.version_info[0] == 2:
        import imp
        task_mod = imp.load_source(mod_name, task_code_path)
    else:
        raise Exception('Unsupported python version: {}'.format(sys.version))
    return task_mod


#get the task property dict; create new if not exists
def get_task_property_dict(input_output_dict):
    if not TASK_OPRPERTY_DF_NAME in input_output_dict:
        input_output_dict[TASK_OPRPERTY_DF_NAME]={}
    return input_output_dict[TASK_OPRPERTY_DF_NAME]

def get_task_property(input_output_dict, name):
    if name not in get_task_property_dict(input_output_dict):
        logger.debug('Property {} was not found in task_property_dict. None value was returned.')
        return None
    else:
        return get_task_property_dict(input_output_dict)[name]


def get_task_property_keyset(input_output_dict):
    return get_task_property_dict(input_output_dict).keys()

# returns old value in property dataframe by name
def add_task_property(input_output_dict, name, value):
    old_val = get_task_property(input_output_dict, name)
    if old_val:
        #remove_task_property(task_property_df, name)
        #logger.debug('Found property name: {} value: {}. Replaced with new value {}.'.format(name, old_val, value))
        print('Found property name: {} value: {}. Replaced with new value {}.'.format(name, old_val, value))
    else:
        logger.debug('added new task property name: {} value: {}.'.format(name, value))
    get_task_property_dict(input_output_dict)[name] = value


def remove_task_property(input_output_dict, name):
    value = get_task_property(input_output_dict, name)
    if value:
        get_task_property_dict(input_output_dict).pop(name)
        logger.debug('Removed task property name: {} value: {}'.format(name, value))
    return value


def get_task_rank(input_output_dict):
    return get_task_property(input_output_dict, 'rank')
